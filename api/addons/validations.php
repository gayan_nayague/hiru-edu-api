<?php
  $response;
  class Validations {
    function isPhone($str) {
      if(preg_match('/^[0-9]{10}+$/', $str)) {
        $response = (object) [
          "valid" => true,
          "message" => $str
        ];
      } else {
        $response = (object) [
          "valid" => false,
          "message" => "Invalid phone number or format"
        ];
      }
      return $response;
    }
    function isEmail($str) {
      if(filter_var($str, FILTER_VALIDATE_EMAIL)) {
        $response = (object) [
          "valid" => true,
          "message" => $str
        ];
      } else {
        $response = (object) [
          "valid" => false,
          "message" => "Invalid email address or format"
        ];
      }
      return $response;
    }
    function isName($str) {
      if(preg_match('/^[A-Za-z]+$/', $str)) {
        $response = (object) [
          "valid" => true,
          "message" => $str
        ];
      } else {
        $response = (object) [
          "valid" => false,
          "message" => "Invalid name or format"
        ];
      }
      return $response;
    }
    function isInt($int) {
      if(filter_var($int, FILTER_VALIDATE_INT)) {
        $response = (object) [
          "valid" => true,
          "message" => $int
        ];
      } else {
        $response = (object) [
          "valid" => false,
          "message" => "Invalid value"
        ];
      }
      return $response;
    }
  }
  return $Validations = new Validations;
?>