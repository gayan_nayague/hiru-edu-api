<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class CMS {
    function getUnreferredVideos() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');

      if ( empty($uid) ) {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      } else {
        $db = new Connect;
        $query = "SELECT COUNT(*) AS is_allowed FROM users
        WHERE uid = '$uid' AND (role_id = 0 OR role_id = 3)";

        $statement = $db->prepare($query);
        $statement->execute();
        $row = $statement->fetch();
        $is_allowed = json_encode($row["is_allowed"]);
        $is_allowed = (int)$is_allowed;

        if ($is_allowed == 1) {

          $unreferred_videos_query = "SELECT * FROM lesson_videos WHERE lesson_id IS NULL AND created_at < NOW() - INTERVAL 2 DAY";
          $unreferred_videos_statement = $db->prepare($unreferred_videos_query);
          $unreferred_videos_statement->execute();
          
          $videosData = array();
          while($OutputData=$unreferred_videos_statement->fetch(PDO::FETCH_ASSOC)){
            $videosData[$OutputData['id']]=array(
            'id'=> $OutputData['id'],
            'created_at' => $OutputData['created_at'],
            'file_name' => $OutputData['file_name']
            );
          };
          $videosData = array_values($videosData);

          $response = array(
            "status" => "success",
            "error" => false,
            "message" => "Successfully created",
            "videos" => $videosData
          );
          http_response_code(200);
          echo json_encode($response);
        } else {
          http_response_code(403);
          echo json_encode(array("message" => "unauthorized"));
        }
      }
    }
  }
  $CMS = new CMS;
  echo $CMS->getUnreferredVideos();
?>