<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class CMS {
    function deleteVideo() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $videos = htmlspecialchars($data->videos, ENT_QUOTES, 'UTF-8');
      $length = count($videos);

      if ( empty($uid) ) {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      } else if ( empty($length > 0) ) {
        http_response_code(400);
        echo json_encode(array("message" => "video is not selected."));
      } else {
        $db = new Connect;
        $query = "SELECT COUNT(*) AS is_allowed FROM users
        WHERE uid = '$uid' AND (role_id = 0 OR role_id = 3)";

        $statement = $db->prepare($query);
        $statement->execute();
        $row = $statement->fetch();
        $is_allowed = json_encode($row["is_allowed"]);
        $is_allowed = (int)$is_allowed;

        if ($is_allowed == 1) {
          $count = 1;
          $error_bag = [];
          foreach($videos as $video) {
            $video_id = $video->id;
            $file_name = $video->file_name;
            $file_with_path = '../../../../uploads/lessons/videos/' . $file_name;
            $unreferred_videos_query = "DELETE FROM lesson_videos WHERE id = $video_id";
            $unreferred_videos_statement = $db->prepare($unreferred_videos_query);
            $unreferred_videos_statement->execute();
            if (!unlink($file_with_path)) {
              array_push($error_bag, $file_name);
            }
            $error_length = count($error_bag);
            if ($length == $count) {
              if ($error_length > 0) {
                $response = array(
                  "status" => "success",
                  "error" => true,
                  "message" => "Error deleting files",
                  "files" => $error_bag
                );
                http_response_code(400);
                echo json_encode($response);
              } else {
                $response = array(
                  "status" => "success",
                  "error" => false,
                  "message" => "Successfully deleted"
                );
                http_response_code(200);
                echo json_encode($response);
              }
            } else {
              $response = array(
                "status" => "success",
                "error" => true,
                "message" => "Error deleting"
              );
              http_response_code(400);
              echo json_encode($response);
            }
            $count++;
          }
        } else {
          http_response_code(403);
          echo json_encode(array("message" => "unauthorized"));
        }
      }
    }
  }
  $CMS = new CMS;
  echo $CMS->deleteVideo();
?>