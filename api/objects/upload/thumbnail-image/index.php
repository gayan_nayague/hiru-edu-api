<?php
  require_once __DIR__ . '../../../../config/core.php';
  header("Content-Type: multipart/form-data");
  require_once __DIR__ . '../../../../config/database.php';

  class UploadImage {
    function uploadThumbnail() {
      try {
        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
          !isset($_FILES['file']['error']) ||
          is_array($_FILES['file']['error'])
        ) {
          throw new RuntimeException('Invalid parameters.');
        }
      
        // Check $_FILES['file']['error'] value.
        switch ($_FILES['file']['error']) {
          case UPLOAD_ERR_OK:
            break;
          case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
          case UPLOAD_ERR_INI_SIZE:
          case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
          default:
            throw new RuntimeException('Unknown errors.');
        }
      
        // You should also check filesize here. 
        if ($_FILES['file']['size'] > 1000000) {
          throw new RuntimeException('Exceeded filesize limit.');
        }
      
        // DO NOT TRUST $_FILES['file']['mime'] VALUE !!
        // Check MIME Type by yourself.
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
          $finfo->file($_FILES['file']['tmp_name']),
          array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png'
          ),
          true
        )) {
          throw new RuntimeException('Invalid file format.');
        }
        $file_name = sha1_file($_FILES['file']['tmp_name']);
        $file_meme_type = $finfo->file($_FILES['file']['tmp_name']);
        $meme_type = '';
        if ($file_meme_type == 'image/jpeg') {
          $file_name = $file_name . '.jpg';
          $meme_type = 'jpg';
        } else if ($file_meme_type == 'image/png') {
          $file_name = $file_name . '.png';
          $meme_type = 'png';
        }

        // You should name it uniquely.
        // DO NOT USE $_FILES['file']['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.

        $upload_directory = './../../../../uploads/lessons/thumbnails';
        // $instructor_directory = $upload_directory . '/' . $instructor_uid . '/lessons/thumbnails';

        if (!file_exists($upload_directory)) {
          mkdir($upload_directory, 0777, true);
        }

        if (!move_uploaded_file(
          $_FILES['file']['tmp_name'],
          sprintf($upload_directory . '/%s.%s',
            sha1_file($_FILES['file']['tmp_name']),
            $ext
          )
        )) {
          throw new RuntimeException('Failed to move uploaded file.');
        }
        self::insertImageRecord($file_name);
      } catch (RuntimeException $e) {
        $response = array(
          "status" => "error",
          "error" => true,
          "message" => $e->getMessage()
        ); 
        echo json_encode($response);
      }
    }
    static function insertImageRecord($file_name) {
      $db = new Connect;
      $query = "CALL `addLessonThumbnail`(:file_name)";

      $statement = $db->prepare($query);
      try {
        if (
          $statement->execute(
            [
              'file_name' => $file_name
            ]
          )
        ) {
          // $response = array(
          //   "status" => "success",
          //   "error" => false,
          //   "message" => "File uploaded successfully",
          //   "file_name" => $file_name
          // );
          // http_response_code(200);
          // echo json_encode($response);
          self::getSavedImage($file_name);
        }
      } catch (Exception $e) {
        $db->rollback();
        throw $e;  
        // set response code - 503 service unavailable
        http_response_code(503);
        // tell the user
        echo json_encode(array("message" => "Unable to record image file."));
      }
    }
    static function getSavedImage($file_name) {
      $db = new Connect;
      $statement = $db->prepare('SELECT id, file_name FROM lesson_thumbnails WHERE file_name = :file_name');
      try {
        if (
          $statement->execute(
            ['file_name' => $file_name]
          )
        ) {
          $thumb_object = $statement->fetch();
          $response = array(
            "status" => "success",
            "error" => false,
            "message" => "File uploaded successfully",
            "object" => $thumb_object
          );
          http_response_code(200);
          echo json_encode($response);
        }
      } catch (Exception $e) {
        $db->rollback();
        throw $e;  
        // set response code - 503 service unavailable
        http_response_code(503);
        // tell the user
        echo json_encode(array("message" => "Unable to record image file."));
      }
    }
  }
  $UploadImage = new UploadImage;
  echo $UploadImage->uploadThumbnail();
?>