<?php
  require_once __DIR__ . '../../../../config/core.php';
  header("Content-Type: multipart/form-data");
  require_once __DIR__ . '../../../../config/database.php';
  include './../../../../vendor/autoload.php';

  use Dilab\Network\SimpleRequest;
  use Dilab\Network\SimpleResponse;
  use Dilab\Resumable;
  
  class UploadVideo {
    function uploadLesson() {
      try {
        $request = new SimpleRequest();
        $response = new SimpleResponse();
        $resumable = new Resumable($request, $response);

        $chunk_directory = './../../../../uploads/lessons/temp_chunks';
        $videos_directory = './../../../../uploads/lessons/videos';
        $resumable->tempFolder = $chunk_directory;
        $resumable->uploadFolder = $videos_directory;
        
        if (!file_exists($chunk_directory)) {
          mkdir($chunk_directory, 0777, true);
        }
              
        if (!file_exists($videos_directory)) {
          mkdir($videos_directory, 0777, true);
        }

        $originalName = $resumable->getOriginalFilename(Resumable::WITHOUT_EXTENSION);
        $slugifiedname = self::uniqueFileName($originalName);
        $resumable->setFilename($slugifiedname);

        $resumable->process();

        if (true === $resumable->isUploadComplete()) {
          $extension = $resumable->getExtension();
          $filename = $resumable->getFilename();
          $last_filename = $filename . '.' . $extension;

          self::insertVideoRecord($last_filename);
        }
        else {
          throw new RuntimeException('Not uploaded successfully');
        }
      } catch (RuntimeException $e) {
        // $response = array(
        //   "status" => "error",
        //   "error" => true,
        //   "message" => $e->getMessage()
        // ); 
        // return json_encode($response);
      }
    }
    static function uniqueFileName($originalName) {
      return md5($originalName . microtime());
    }
    static function insertVideoRecord($file_name) {
      $db = new Connect;
      $query = "CALL `addVideoFile`(:file_name)";

      $statement = $db->prepare($query);
      try {
        if (
          $statement->execute(
            [
              'file_name' => $file_name
            ]
          )
        ) {
          // $response = array(
          //   "status" => "success",
          //   "error" => false,
          //   "message" => "File uploaded successfully",
          //   "file_name" => $file_name
          // );
          // http_response_code(200);
          // echo json_encode($response);
          self::getSavedVideo($file_name);
        }
      } catch (Exception $e) {
        $db->rollback();
        throw $e;  
        // set response code - 503 service unavailable
        http_response_code(503);
        // tell the user
        echo json_encode(array("message" => "Unable to add the video file to database."));
      }
    }
    static function getSavedVideo($file_name) {
      $db = new Connect;
      $statement = $db->prepare('SELECT id, file_name FROM lesson_videos WHERE file_name = :file_name');
      try {
        if (
          $statement->execute(
            ['file_name' => $file_name]
          )
        ) {
          $thumb_object = $statement->fetch();
          $response = array(
            "status" => "success",
            "error" => false,
            "message" => "File uploaded successfully",
            "object" => $thumb_object
          );
          http_response_code(200);
          echo json_encode($response);
        }
      } catch (Exception $e) {
        $db->rollback();
        throw $e;  
        // set response code - 503 service unavailable
        http_response_code(503);
        // tell the user
        echo json_encode(array("message" => "Unable to add the video file to database."));
      }
    }
  }
  $UploadVideo = new UploadVideo;
  echo $UploadVideo->uploadLesson();
?>