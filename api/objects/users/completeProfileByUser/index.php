<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class User {
    function completeProfileByUser() {
      $data = json_decode(file_get_contents("php://input"));

      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $firstname = htmlspecialchars($data->firstname, ENT_QUOTES, 'UTF-8');
      $lastname = htmlspecialchars($data->lastname, ENT_QUOTES, 'UTF-8');
      $phone = htmlspecialchars($data->phone, ENT_QUOTES, 'UTF-8');
      
      $Validations = new Validations;

      if (empty($uid)) {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      } else if (empty($firstname)) {
        http_response_code(400);
        echo json_encode(array("message" => "firstname is undefined."));
      } else if (!$Validations->isName($firstname)->valid) {
        http_response_code(400);
        echo json_encode(array("message" => $Validations->isName($firstname)->message));
      } else if (empty($lastname)) {
        http_response_code(400);
        echo json_encode(array("message" => "lastname is undefined."));
      } else if (!$Validations->isName($lastname)->valid) {
        http_response_code(400);
        echo json_encode(array("message" => $Validations->isName($lastname)->message));
      } else if (empty($phone)) {
        http_response_code(400);
        echo json_encode(array("message" => "phone number is undefined."));
      } else if (!$Validations->isPhone($phone)->valid) {
        http_response_code(400);
        echo json_encode(array("message" => $Validations->isPhone($phone)->message));
      } else {
        $db = new Connect;
        $query = "UPDATE users SET firstname = '$firstname', lastname = '$lastname', phone = '$phone', completed = 1  WHERE uid = '$uid'";
        $statement = $db->prepare($query);

        try {
          if (
            $statement->execute()
          ) {
            $response = array(
              "status" => "success",
              "error" => false,
              "message" => "User updated"
            );
            http_response_code(200);
            echo json_encode($response);
          }
        } catch (Exception $e) {
          $db->rollback();
          throw $e;
          // set response code - 503 service unavailable
          http_response_code(503);
          // tell the user
          echo json_encode(array("message" => "Unable to update user."));
        }
      }
    }
  }
  $User = new User;
  echo $User->completeProfileByUser();
?>