<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class User {
    function getUser() {
      $data = json_decode(file_get_contents("php://input"));

      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');

      if (
        !empty($uid)
      ) {
        $db = new Connect;
        $query = "CALL `getUser`(:uid)";
        $statement = $db->prepare($query);
        $statement->execute([
          'uid' => $uid
        ]);
        $userData = array();
        while($OutputData=$statement->fetch(PDO::FETCH_ASSOC)){
          $userData[$OutputData['id']]=array(
           'id'=> $OutputData['id'],
           'uid' => $OutputData['uid'],
           'firstname' => $OutputData['firstname'],
           'lastname' => $OutputData['lastname'],
           'email' => $OutputData['email'],
           'phone' => $OutputData['phone'],
           'photoURL' => $OutputData['photoURL'],
           'creationTime' => $OutputData['creationTime'],
           'completed' => $OutputData['completed'],
           'roleTypeId' => $OutputData['roleTypeId'],
           'roleName' => $OutputData['roleName'],
           'bio' => $OutputData['bio'],
           'followers_count' => $OutputData['followers_count'],
           'lessons_count' => $OutputData['lessons_count'],
           'student_count' => $OutputData['student_count'],
          );
        }
        $userData = array_values($userData);
        return json_encode($userData);
      } else {
        http_response_code(400);    
        // tell the user
        echo json_encode(array("message" => "uid is undefined."));
      }
    }
  }
  $User = new User;
  echo $User->getUser();
?>