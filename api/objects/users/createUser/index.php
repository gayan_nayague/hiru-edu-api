<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class User {
    function createUser() {
      $data = json_decode(file_get_contents("php://input"));

      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $firstname = !empty($data->firstname) ? htmlspecialchars($data->firstname, ENT_QUOTES, 'UTF-8') : '';
      $lastname = !empty($data->lastname) ? htmlspecialchars($data->lastname, ENT_QUOTES, 'UTF-8') : '';
      $email = !empty($data->email) ? htmlspecialchars($data->email, ENT_QUOTES, 'UTF-8') : '';
      $phone = !empty($data->phone) ? htmlspecialchars($data->phone, ENT_QUOTES, 'UTF-8') : '';
      $photoURL = !empty($data->photoURL) ? htmlspecialchars($data->photoURL, ENT_QUOTES, 'UTF-8') : '';
      $creationTime = !empty($data->creationTime) ? htmlspecialchars($data->creationTime, ENT_QUOTES, 'UTF-8') : '';

      $Validations = new Validations;

      if (
        !empty($uid)
      ) {
        $db = new Connect;
        $query = "CALL `addUser`(:uid, :firstname, :lastname, :email, :phone, :photoURL, :creationTime)";
        $statement = $db->prepare($query);

        if (empty($uid)) {
          http_response_code(400);
          echo json_encode(array("message" => "uid is undefined."));
        } else if (empty($firstname)) {
          http_response_code(400);
          echo json_encode(array("message" => "firstname is undefined."));
        } else if (!$Validations->isName($firstname)->valid) {
          http_response_code(400);
          echo json_encode(array("message" => $Validations->isName($firstname)->message));
        } else if (empty($lastname)) {
          http_response_code(400);
          echo json_encode(array("message" => "lastname is undefined."));
        } else if (!$Validations->isName($lastname)->valid) {
          http_response_code(400);
          echo json_encode(array("message" => $Validations->isName($lastname)->message));
        } else if (empty($email)) {
          http_response_code(400);
          echo json_encode(array("message" => "email is undefined."));
        } else if (!$Validations->isEmail($email)->valid) {
          http_response_code(400);
          echo json_encode(array("message" => $Validations->isEmail($email)->message));
        } else if (!empty($phone) && !$Validations->isPhone($phone)->valid) {
          http_response_code(400);
          echo json_encode(array("message" => $Validations->isPhone($phone)->message));
        } else {
          try {
            if (
              $statement->execute(
                [
                  'uid' => $uid,
                  'firstname' => $firstname,
                  'lastname' => $lastname,
                  'email' => $email,
                  'phone' => $phone,
                  'photoURL' => $photoURL,
                  'creationTime' => $creationTime
                ]
              )
            ) {
              // set response code - 201 created
              http_response_code(201);
              // tell the user
              echo json_encode(array("message" => "User registered."));
            }
          } catch (Exception $e) {
            $db->rollback();
            throw $e;
            // set response code - 503 service unavailable
            http_response_code(503);
            // tell the user
            echo json_encode(array("message" => "Unable to register user."));
          }
        }
      } else {
        http_response_code(400);    
        // tell the user
        echo json_encode(array("message" => "uid is undefined."));
      }
    }
  }
  $User = new User;
  echo $User->createUser();
?>