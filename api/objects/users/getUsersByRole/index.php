<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class User {
    function getUser() {
      $data = json_decode(file_get_contents("php://input"));

      $roleTypeId = htmlspecialchars($data->roleTypeId, ENT_QUOTES, 'UTF-8');

      if (
        !empty($roleTypeId)
      ) {
        $db = new Connect;
        $query = "CALL `getUsersByRole`(:roleTypeId)";
        $statement = $db->prepare($query);
        $statement->execute([
          'roleTypeId' => $roleTypeId
        ]);
        $userData = array();
        while($OutputData=$statement->fetch(PDO::FETCH_ASSOC)){
          $userData[$OutputData['id']]=array(
           'id'=> $OutputData['id'],
           'uid' => $OutputData['uid'],
           'firstname' => $OutputData['firstname'],
           'lastname' => $OutputData['lastname'],
           'email' => $OutputData['email'],
           'phone' => $OutputData['phone'],
           'photoURL' => $OutputData['photoURL'],
           'creationTime' => $OutputData['creationTime'],
           'roleTypeId' => $OutputData['roleTypeId'],
           'roleName' => $OutputData['roleName']
          );
        }
        $userData = array_values($userData);
        return json_encode($userData);
      } else {
        http_response_code(400);    
        // tell the user
        echo json_encode(array("message" => "roleTypeId is undefined."));
      }
    }
  }
  $User = new User;
  echo $User->getUser();
?>