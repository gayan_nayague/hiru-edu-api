<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class Send {
    function sendEmail() {
      $data = json_decode(file_get_contents("php://input"));

      $email = !empty($data->email) ? htmlspecialchars($data->email, ENT_QUOTES, 'UTF-8') : '';
      $phone = !empty($data->phone) ? htmlspecialchars($data->phone, ENT_QUOTES, 'UTF-8') : '';
      $name = !empty($data->name) ? htmlspecialchars($data->name, ENT_QUOTES, 'UTF-8') : '';
      $subject = !empty($data->subject) ? htmlspecialchars($data->subject, ENT_QUOTES, 'UTF-8') : '';
      $body = !empty($data->body) ? htmlspecialchars($data->body, ENT_QUOTES, 'UTF-8') : '';

      $Validations = new Validations;

      if (empty($subject)) {
        http_response_code(400);
        echo json_encode(array("message" => "subject is undefined."));
      } else if (empty($body)) {
        http_response_code(400);
        echo json_encode(array("message" => "body is undefined."));
      } else if (empty($name)) {
        http_response_code(400);
        echo json_encode(array("message" => "name is undefined."));
      } else if (empty($email)) {
        http_response_code(400);
        echo json_encode(array("message" => "email is undefined."));
      } else if (!$Validations->isEmail($email)->valid) {
        http_response_code(400);
        echo json_encode(array("message" => $Validations->isEmail($email)->message));
      } else if (!empty($phone) && !$Validations->isPhone($phone)->valid) {
        http_response_code(400);
        echo json_encode(array("message" => $Validations->isPhone($phone)->message));
      } else {
        $content = "
        To whom may concern,\n\n
        Name:".$name."\n
        Phone: ".$phone."\n\n
        Body: ".$body;
        mail($email, $subject, $content);
        http_response_code(200);
        echo json_encode(array("message" => "Email is successfully sent"));
      }
    }
  }
  $Send = new Send;
  echo $Send->sendEmail();
?>