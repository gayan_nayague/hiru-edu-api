<?php
  require_once __DIR__ . '../../../../../config/core.php';
  require_once __DIR__ . '../../../../../config/database.php';
  class CMS {
    function getCMSClasses() {
      $db = new Connect;
      $classes_query = "SELECT * FROM classes";
      $classes_statement = $db->prepare($classes_query);
      $classes_statement->execute();
      $classesData = array();
      while($OutputData=$classes_statement->fetch(PDO::FETCH_ASSOC)){
        $classesData[$OutputData['id']]=array(
          'id'=> $OutputData['id'],
          'class_name' => $OutputData['class_name']
        );
      }
      http_response_code(200);
      $classesData = array_values($classesData);
      return json_encode($classesData);
    }
  }
  $CMS = new CMS;
  echo $CMS->getCMSClasses();
?>