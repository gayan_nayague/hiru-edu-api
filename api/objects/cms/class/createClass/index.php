<?php
  require_once __DIR__ . '../../../../../config/core.php';
  require_once __DIR__ . '../../../../../config/database.php';
  class CMS {
    function createClass() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $class_name = htmlspecialchars($data->class_name, ENT_QUOTES, 'UTF-8');

      if ( empty($uid) ) {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      } else if ( empty($class_name) ) {
        http_response_code(400);
        echo json_encode(array("message" => "class name is empty."));
      } else {
        $db = new Connect;
        $query = "SELECT COUNT(*) AS is_admin FROM users
        WHERE uid = '$uid' AND role_id = 0";

        $statement = $db->prepare($query);
        $statement->execute();
        $row = $statement->fetch();
        $is_admin = json_encode($row["is_admin"]);
        $is_admin = (int)$is_admin;

        if ($is_admin == 1) {

          $classes_query = "INSERT INTO classes (class_name) VALUES('$class_name')";
          $classes_statement = $db->prepare($classes_query);
          $classes_statement->execute();
          $response = array(
            "status" => "success",
            "error" => false,
            "message" => "Successfully created"
          );
          http_response_code(200);
          echo json_encode($response);
        } else {
          http_response_code(403);
          echo json_encode(array("message" => "unauthorized"));
        }
      }
    }
  }
  $CMS = new CMS;
  echo $CMS->createClass();
?>