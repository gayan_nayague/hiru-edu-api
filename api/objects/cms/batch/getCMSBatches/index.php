<?php
  require_once __DIR__ . '../../../../../config/core.php';
  require_once __DIR__ . '../../../../../config/database.php';
  class CMS {
    function getCMSBatches() {
      $db = new Connect;
      $bacthes_query = "SELECT * FROM batch";
      $batches_statement = $db->prepare($bacthes_query);
      $batches_statement->execute();
      $batchesData = array();
      while($OutputData=$batches_statement->fetch(PDO::FETCH_ASSOC)){
        $batchesData[$OutputData['id']]=array(
          'id'=> $OutputData['id'],
          'batch' => $OutputData['batch']
        );
      }
      http_response_code(200);
      $batchesData = array_values($batchesData);
      return json_encode($batchesData);
    }
  }
  $CMS = new CMS;
  echo $CMS->getCMSBatches();
?>