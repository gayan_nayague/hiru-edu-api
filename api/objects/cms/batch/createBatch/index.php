<?php
  require_once __DIR__ . '../../../../../config/core.php';
  require_once __DIR__ . '../../../../../config/database.php';
  class CMS {
    function createBatch() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $year = htmlspecialchars($data->year, ENT_QUOTES, 'UTF-8');

      if ( empty($uid) ) {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      } else if ( empty($year) ) {
        http_response_code(400);
        echo json_encode(array("message" => "year is undefined."));
      } else {
        $db = new Connect;
        $query = "SELECT COUNT(*) AS is_admin FROM users
        WHERE uid = '$uid' AND role_id = 0";

        $statement = $db->prepare($query);
        $statement->execute();
        $row = $statement->fetch();
        $is_admin = json_encode($row["is_admin"]);
        $is_admin = (int)$is_admin;

        if ($is_admin == 1) {

          $bacthes_query = "INSERT INTO batch (batch) VALUES('$year')";
          $batches_statement = $db->prepare($bacthes_query);
          $batches_statement->execute();
          $response = array(
            "status" => "success",
            "error" => false,
            "message" => "Successfully created"
          );
          http_response_code(200);
          echo json_encode($response);
        } else {
          http_response_code(403);
          echo json_encode(array("message" => "unauthorized"));
        }
      }
    }
  }
  $CMS = new CMS;
  echo $CMS->createBatch();
?>