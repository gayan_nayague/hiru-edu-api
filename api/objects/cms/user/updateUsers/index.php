<?php
  require_once __DIR__ . '../../../../../config/core.php';
  require_once __DIR__ . '../../../../../config/database.php';
  class CMS {
    function updateUser() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $selected_user_uid = htmlspecialchars($data->selected_user_uid, ENT_QUOTES, 'UTF-8');
      $firstname = htmlspecialchars($data->firstname, ENT_QUOTES, 'UTF-8');
      $lastname = htmlspecialchars($data->lastname, ENT_QUOTES, 'UTF-8');
      $email = htmlspecialchars($data->email, ENT_QUOTES, 'UTF-8');
      $phone = htmlspecialchars($data->phone, ENT_QUOTES, 'UTF-8');
      $roleTypeId = htmlspecialchars($data->roleTypeId, ENT_QUOTES, 'UTF-8');

      if ( empty($uid) ) {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      } else if ( empty($selected_user_uid) ) {
        http_response_code(400);
        echo json_encode(array("message" => "User not selected."));
      } else {
        $db = new Connect;
        $query_admin_or_manager = "SELECT COUNT(*) AS is_admin_or_manager FROM users WHERE uid = '$uid' AND (role_id = 0 OR role_id = 3)";
        $query_admin = "SELECT COUNT(*) AS is_admin FROM users WHERE uid = '$uid' AND role_id = 0";

        $admin_or_manager_statement = $db->prepare($query_admin_or_manager);
        $admin_statement = $db->prepare($query_admin);

        $admin_or_manager_statement->execute();
        $admin_statement->execute();

        $admin_or_manager_row = $admin_or_manager_statement->fetch();
        $admin_row = $admin_statement->fetch();

        $is_admin_or_manager = json_encode($admin_or_manager_row["is_admin_or_manager"]);
        $is_admin = json_encode($admin_row["is_admin"]);

        $is_admin_or_manager = (int)$is_admin_or_manager;
        $is_admin = (int)$is_admin;

        if ($is_admin == 1) {

          $user_query = "UPDATE users SET firstname = '$firstname', lastname = '$lastname', email = '$email', phone = '$phone', role_id = '$roleTypeId' WHERE uid = '$selected_user_uid'";

          $user_statement = $db->prepare($user_query);
          $user_statement->execute();
          $response = array(
            "status" => "success",
            "error" => false,
            "message" => "Successfully updated role"
          );
          http_response_code(200);
          echo json_encode($response);
        } elseif ($is_admin_or_manager == 1) {

          $user_query = "UPDATE users SET firstname = '$firstname', lastname = '$lastname', email = '$email', phone = '$phone' WHERE uid = '$selected_user_uid'";

          $user_statement = $db->prepare($user_query);
          $user_statement->execute();
          $response = array(
            "status" => "success",
            "error" => false,
            "message" => "Successfully updated"
          );
          http_response_code(200);
          echo json_encode($response);
        } else {
          http_response_code(403);
          echo json_encode(array("message" => "unauthorized"));
        }
      }
    }
  }
  $CMS = new CMS;
  echo $CMS->updateUser();
?>