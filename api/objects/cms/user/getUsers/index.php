<?php
  require_once __DIR__ . '../../../../../config/core.php';
  require_once __DIR__ . '../../../../../config/database.php';
  class CMS {
    function getUsers() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');

      if ( empty($uid) ) {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      } else {
        $db = new Connect;
        $query = "SELECT COUNT(*) AS is_allowed FROM users WHERE uid = '$uid' AND (role_id = 0 OR role_id = 3)";

        $statement = $db->prepare($query);
        $statement->execute();
        $row = $statement->fetch();
        $is_allowed = json_encode($row["is_allowed"]);
        $is_allowed = (int)$is_allowed;

        if ($is_allowed == 1) {

          $users_query = "SELECT user.id, user.uid, user.firstname, user.lastname, user.email, user.phone, user.photoURL, user.creationTime, user.completed, role.roleTypeId, role.roleName, meta.bio, meta.followers_count, meta.lessons_count, meta.student_count FROM users AS user
          LEFT JOIN user_roles AS role
          ON user.role_id = role.roleTypeId
          LEFT JOIN user_meta AS meta
          ON user.uid= meta.uid";
          $users_statement = $db->prepare($users_query);
          $users_statement->execute();
          $userData = array();
          while($OutputData=$users_statement->fetch(PDO::FETCH_ASSOC)){
            $userData[$OutputData['id']]=array(
            //  'id'=> $OutputData['id'],
             'uid' => $OutputData['uid'],
             'firstname' => $OutputData['firstname'],
             'lastname' => $OutputData['lastname'],
             'email' => $OutputData['email'],
             'phone' => $OutputData['phone'],
            //  'photoURL' => $OutputData['photoURL'],
             'creationTime' => $OutputData['creationTime'],
             'completed' => $OutputData['completed'],
            //  'roleTypeId' => $OutputData['roleTypeId'],
             'roleName' => $OutputData['roleName'],
            //  'bio' => $OutputData['bio'],
            //  'followers_count' => $OutputData['followers_count'],
            //  'lessons_count' => $OutputData['lessons_count'],
            //  'student_count' => $OutputData['student_count'],
            );
          }
          $userData = array_values($userData);
          http_response_code(200);
          return json_encode($userData);
        } else {
          http_response_code(403);
          echo json_encode(array("message" => "unauthorized"));
        }
      }
    }
  }
  $CMS = new CMS;
  echo $CMS->getUsers();
?>