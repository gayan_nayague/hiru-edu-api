<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class EnrolledLessons {
    function getEnrolledLessonList() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      if (
        !empty($uid)
      ) {
        $db = new Connect;
        $query = "SELECT * FROM enrolled_lessons WHERE user_uid = :uid AND enrolled = 1";
        $statement = $db->prepare($query);
        $statement->execute([
          'uid' => $uid
        ]);
        $lessonsData = array();
        while($OutputData=$statement->fetch(PDO::FETCH_ASSOC)){
          $lessonsData[$OutputData['id']]=array(
           'id'=> $OutputData['id'],
           'user_uid' => $OutputData['user_uid'],
           'lesson_id' => $OutputData['lesson_id'],
           'created_at' => $OutputData['created_at']
          );
        }
        $lessonsData = array_values($lessonsData);
        http_response_code(200);
        return json_encode($lessonsData);
      } else {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      }
    }
  }
  $EnrolledLessons = new EnrolledLessons;
  echo $EnrolledLessons->getEnrolledLessonList();
?>