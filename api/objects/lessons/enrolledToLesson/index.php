<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class EnrollLessons {
    function enrollToLesson() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $lesson_id = htmlspecialchars($data->lesson_id, ENT_QUOTES, 'UTF-8');
      if (empty($uid)) {
        http_response_code(400);
        echo json_encode(array("message" => "user identity is undefined."));
      } elseif (empty($lesson_id)) {
        http_response_code(400);
        echo json_encode(array("message" => "lesson id is undefined."));
      } else {
        $db = new Connect;
        
        $is_exists_query = "SELECT COUNT(*) AS is_exists FROM enrolled_lessons WHERE user_uid = '$uid' AND lesson_id = '$lesson_id'";
        $is_exists_statement = $db->prepare($is_exists_query);
        $is_exists_statement->execute();
        $is_exists_row = $is_exists_statement->fetch();
        if (!empty($is_exists_row["is_exists"])) {
          $set_enroll_query = "UPDATE enrolled_lessons SET enrolled = 1 WHERE user_uid = '$uid' AND lesson_id = '$lesson_id'";
          $set_enroll_statement = $db->prepare($set_enroll_query);
          $set_enroll_statement->execute();

          $videos_query = "SELECT id FROM lesson_videos WHERE lesson_id = '$lesson_id'";
          $videos_statement = $db->prepare($videos_query);
          $videos_statement->execute();

          $videos = array();
          while($OutputData=$videos_statement->fetch(PDO::FETCH_ASSOC)){
            $videos[$OutputData['id']]=array(
              'id'=> $OutputData['id']
            );
          }
          $videos = array_values($videos);
          foreach($videos as $video) {
            $video_id = $video['id'];
            $set_attempts_query = "UPDATE video_attempts SET attempts = 2 WHERE uid = '$uid' AND lesson_id = '$lesson_id' AND video_id = '$video_id'";
            $set_attempts_statement = $db->prepare($set_attempts_query);
            $set_attempts_statement->execute();
          }
          $response = array(
            "status" => "success",
            "error" => false,
            "user_id" => $uid,
            "lesson_id" => $lesson_id
          );
          http_response_code(200);
          echo json_encode($response);
        } else {
          $query = "INSERT INTO enrolled_lessons (user_uid, lesson_id) VALUES('$uid', '$lesson_id')";
          $statement = $db->prepare($query);
          $statement->execute();
  
          $videos_query = "SELECT id FROM lesson_videos WHERE lesson_id = '$lesson_id'";
          $videos_statement = $db->prepare($videos_query);
          $videos_statement->execute();
  
          $videos = array();
          while($OutputData=$videos_statement->fetch(PDO::FETCH_ASSOC)){
            $videos[$OutputData['id']]=array(
              'id'=> $OutputData['id']
            );
          }
          $videos = array_values($videos);
          foreach($videos as $video) {
            $video_id = $video['id'];
            $add_attempts_query = "INSERT INTO video_attempts (uid, lesson_id, video_id, attempts) VALUES('$uid', '$lesson_id', '$video_id', 2)";
            $add_attempts_statement = $db->prepare($add_attempts_query);
            $add_attempts_statement->execute();
          }
          $response = array(
            "status" => "success",
            "error" => false,
            "user_id" => $uid,
            "lesson_id" => $lesson_id
          );
          http_response_code(200);
          echo json_encode($response);
        }
      }
    }
  }
  $EnrollLessons = new EnrollLessons;
  echo $EnrollLessons->enrollToLesson();
?>