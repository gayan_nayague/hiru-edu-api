<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class EnrollLessons {
    function enrollToLesson() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $lesson_id = htmlspecialchars($data->lesson_id, ENT_QUOTES, 'UTF-8');
      $video_id = htmlspecialchars($data->video_id, ENT_QUOTES, 'UTF-8');

      if (empty($uid)) {
        http_response_code(400);
        echo json_encode(array("message" => "user identity is undefined."));
      } elseif (empty($lesson_id)) {
        http_response_code(400);
        echo json_encode(array("message" => "lesson is undefined."));
      } elseif (empty($video_id)) {
        http_response_code(400);
        echo json_encode(array("message" => "video is undefined."));
      } else {
        $db = new Connect;

        // check for the owner begins
        $owner_query = "SELECT COUNT(*) AS is_owner FROM lessons
        WHERE user_uid = '$uid' AND id = '$lesson_id'";
        $owner_statement = $db->prepare($owner_query);
        $owner_statement->execute();
        $owner_row = $owner_statement->fetch();
        if (!empty($owner_row["is_owner"])) {
          $response = array(
            "status" => "success",
            "error" => false,
            "attempts" => 2
          );
          http_response_code(200);
          echo json_encode($response);
          // check for the owner ends
        } else {
          $is_available_query = "SELECT COUNT(*) AS is_attempts FROM video_attempts WHERE uid = '$uid' AND lesson_id = '$lesson_id' AND video_id = '$video_id'";
          $is_available_statement = $db->prepare($is_available_query);
          $is_available_statement->execute();
          $is_available_row = $is_available_statement->fetch();
          $attempts = 0;
          if (!empty($is_available_row["is_attempts"])) {
            $attempts_query = "SELECT attempts FROM video_attempts WHERE uid = '$uid' AND lesson_id = '$lesson_id' AND video_id = '$video_id'";
            $attempts_statement = $db->prepare($attempts_query);
            $attempts_statement->execute();
            $attempts_row = $attempts_statement->fetch();
            $attempts = json_encode($attempts_row["attempts"]);
            if ($attempts > 0) {
              $update_count = --$attempts;
              $set_attempts_query = "UPDATE video_attempts SET attempts = '$update_count' WHERE uid = '$uid' AND lesson_id = '$lesson_id' AND video_id = '$video_id'";
              $set_attempts_statement = $db->prepare($set_attempts_query);
              $set_attempts_statement->execute();
              $response = array(
                "status" => "success",
                "error" => false,
                "attempts" => $update_count
              );
            } else {
              $response = array(
                "status" => "success",
                "error" => false,
                "attempts" => 0
              );
            }
          } else {
            $response = array(
              "status" => "failed",
              "error" => true,
              "attempts" => 0
            );
          }
          http_response_code(200);
          echo json_encode($response);
        }
      }
    }
  }
  $EnrollLessons = new EnrollLessons;
  echo $EnrollLessons->enrollToLesson();
?>