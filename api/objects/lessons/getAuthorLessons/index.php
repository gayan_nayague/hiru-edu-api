<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class Lessons {
    function getAuthorLessons() {
      $data = json_decode(file_get_contents("php://input"));

      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');

      if (
        !empty($uid)
      ) {
        $db = new Connect;
        $query = "SELECT
        l.id AS l_id,
        l.created_at AS l_created,
        l.updated_at AS l_updated,
        l.user_uid AS l_uid,
        l.title AS l_title,
        l.short_description AS l_desc,
        l.thumbnail_id AS l_thumb_id,
        t.id AS t_id,
        t.file_name AS t_f_name
        FROM lessons AS l
        LEFT JOIN lesson_thumbnails as t
        ON l.thumbnail_id = t.id
        WHERE user_uid = :uid";
        $statement = $db->prepare($query);
        $statement->execute([
          'uid' => $uid
        ]);
        $lessonsData = array();
        while($OutputData=$statement->fetch(PDO::FETCH_ASSOC)){
          $lessonsData[$OutputData['l_id']]=array(
           'l_id'=> $OutputData['l_id'],
           'l_created' => $OutputData['l_created'],
           'l_updated' => $OutputData['l_updated'],
           'l_uid' => $OutputData['l_uid'],
           'l_title' => $OutputData['l_title'],
           'l_desc' => $OutputData['l_desc'],
           't_f_name' => $OutputData['t_f_name'],
          );
        }
        $lessonsData = array_values($lessonsData);
        return json_encode($lessonsData);
      } else {
        http_response_code(400);    
        // tell the user
        echo json_encode(array("message" => "uid is undefined."));
      }
    }
  }
  $Lessons = new Lessons;
  echo $Lessons->getAuthorLessons();
?>