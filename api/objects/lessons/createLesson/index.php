<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class Lesson {
    function createLesson() {
      $data = json_decode(file_get_contents("php://input"));

      $title = !empty($data->title) ? htmlspecialchars($data->title, ENT_QUOTES, 'UTF-8') : '';
      $short_description = !empty($data->short_description) ? htmlspecialchars($data->short_description, ENT_QUOTES, 'UTF-8') : '';
      $assignee_uid = !empty($data->assignee_uid) ? htmlspecialchars($data->assignee_uid, ENT_QUOTES, 'UTF-8') : '';
      $thumbnail_id = !empty($data->thumbnail_id) ? htmlspecialchars($data->thumbnail_id, ENT_QUOTES, 'UTF-8') : '';
      $videos = !empty($data->videos) ? htmlspecialchars($data->videos, ENT_QUOTES, 'UTF-8') : [];
      $tutes = !empty($data->tutes) ? htmlspecialchars($data->tutes, ENT_QUOTES, 'UTF-8') : [];
      $batch_id = !empty($data->batch_id) ? htmlspecialchars($data->batch_id, ENT_QUOTES, 'UTF-8') : '';
      $class_id = !empty($data->class_id) ? htmlspecialchars($data->class_id, ENT_QUOTES, 'UTF-8') : '';
      $created_time = date("Y-m-d H:i:s");

      if (
        !empty($assignee_uid)
      ) {
        $db = new Connect;
        $query = "CALL `addLesson`(:title, :short_description, :assignee_uid, :thumbnail_id, :created_time, :batch_id, :class_id)";
        $statement = $db->prepare($query);

        try {
          if (
            $statement->execute(
              [
                'title' => $title,
                'short_description' => $short_description,
                'assignee_uid' => $assignee_uid,
                'thumbnail_id' => $thumbnail_id,
                'created_time' => $created_time,
                'batch_id' => $batch_id,
                'class_id' => $class_id
              ]
            )
          ) {
            // set response code - 201 created
            // http_response_code(201);
            // tell the user
            // echo json_encode(array("message" => "Lesson added."));
            self::getLastAddedLesson($db, $videos, $tutes);
          }
        } catch (Exception $e) {
          $db->rollback();
          throw $e;
          // set response code - 503 service unavailable
          http_response_code(503);
          // tell the user
          echo json_encode(array("message" => "Unable to add lesson."));
        }
      } else {
        http_response_code(400);
        // tell the user
        echo json_encode(array("message" => "uid is undefined."));
      }
    }
    static function getLastAddedLesson($db, $videos, $tutes) {
      $query = "SELECT * FROM lessons ORDER BY created_at DESC LIMIT 1";
      $statement = $db->prepare($query);
      $statement->execute();
      $row = $statement->fetch();
      $last_id = json_encode($row["id"]);
      self::referVideos($db, $last_id, $videos);
      self::referTutes($db, $last_id, $tutes);
    }
    static function referVideos($db, $last_id, $videos) {
      foreach ($videos as $video) {
        $query = "UPDATE lesson_videos SET video_title = '$video->episode_name', lesson_id = '$last_id', display_order = '$video->display_order' WHERE id = '$video->video_id'";
        $statement = $db->prepare($query);
        $statement->execute();
        // echo $video->file_name . '<br>';
      }
      $response = array(
        "status" => "success",
        "error" => false,
        "message" => "Lesson added successfully"
      );
      http_response_code(200);
      echo json_encode($response);
    }
    static function referTutes($db, $last_id, $tutes) {
      foreach ($tutes as $tute) {
        $query = "UPDATE lesson_tute SET tute_title = '$tute->tute_title', lesson_id = '$last_id', display_order = '$tute->display_order' WHERE id = '$tute->tute_id'";
        $statement = $db->prepare($query);
        $statement->execute();
        echo $tute->tute_name . '<br>';
      }
      echo json_encode('Tute refered successfully');
    }
  }
  $Lesson = new Lesson;
  echo $Lesson->createLesson();
?>