<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class EnrollLessons {
    function checkEnrollment() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $lesson_id = htmlspecialchars($data->lesson_id, ENT_QUOTES, 'UTF-8');
      if (empty($uid)) {
        http_response_code(400);
        echo json_encode(array("message" => "user identity is undefined."));
      } elseif (empty($lesson_id)) {
        http_response_code(400);
        echo json_encode(array("message" => "lesson id is undefined."));
      } else {
        $db = new Connect;

        // check for the owner begins
        $owner_query = "SELECT COUNT(*) AS is_owner FROM lessons
        WHERE user_uid = '$uid' AND id = '$lesson_id'";
        $owner_statement = $db->prepare($owner_query);
        $owner_statement->execute();
        $owner_row = $owner_statement->fetch();
        if (!empty($owner_row["is_owner"])) {
          $response = array(
            "status" => "success",
            "error" => false,
            "enrollement" => 1
          );
          http_response_code(200);
          echo json_encode($response);
          // check for the owner ends
        } else {
          $query = "SELECT COUNT(*) AS enrollement FROM enrolled_lessons
          WHERE user_uid = '$uid' AND lesson_id = '$lesson_id' AND enrolled = 1";
          $statement = $db->prepare($query);
          $statement->execute();
          $row = $statement->fetch();
          $enrollement = 0;
          if (!empty($row["enrollement"])) {
            $enrollement = json_encode($row["enrollement"]);
          }
          $enrollement = (int)$enrollement;
          if ($enrollement) {
            $attempt_query = "SELECT COUNT(*) AS attempts FROM
            video_attempts
            WHERE uid = '$uid' AND lesson_id = '$lesson_id' AND attempts >= 1";
            $attempt_statement = $db->prepare($attempt_query);
            $attempt_statement->execute();
            $attempt_row = $attempt_statement->fetch();
            $attempt = 0;
            if (!empty($attempt_row["attempts"])) {
              $attempt = json_encode($attempt_row["attempts"]);
            }
            if ($attempt === 0) {
              $set_enroll_query = "UPDATE enrolled_lessons SET enrolled = 0 WHERE user_uid = '$uid' AND lesson_id = '$lesson_id'";
              $set_enroll_statement = $db->prepare($set_enroll_query);
              $set_enroll_statement->execute();
              $enrollement = 0;
            }
          }
          $response = array(
            "status" => "success",
            "error" => false,
            "enrollement" => $enrollement
          );
          http_response_code(200);
          echo json_encode($response);
        }
      }
    }
  }
  $EnrollLessons = new EnrollLessons;
  echo $EnrollLessons->checkEnrollment();
?>