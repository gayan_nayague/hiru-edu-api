<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class EnrollLessons {
    function enrollToLesson() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $lesson_id = htmlspecialchars($data->lesson_id, ENT_QUOTES, 'UTF-8');
      $video_id = htmlspecialchars($data->video_id, ENT_QUOTES, 'UTF-8');

      if (empty($uid)) {
        http_response_code(400);
        echo json_encode(array("message" => "user identity is undefined."));
      } elseif (empty($lesson_id)) {
        http_response_code(400);
        echo json_encode(array("message" => "lesson is undefined."));
      } elseif (empty($video_id)) {
        http_response_code(400);
        echo json_encode(array("message" => "video is undefined."));
      } else {
        $db = new Connect;

        // check for the owner begins
        $owner_query = "SELECT COUNT(*) AS is_owner FROM lessons
        WHERE user_uid = '$uid' AND id = '$lesson_id'";
        $owner_statement = $db->prepare($owner_query);
        $owner_statement->execute();
        $owner_row = $owner_statement->fetch();
        if (!empty($owner_row["is_owner"])) {
          $response = array(
            "status" => "success",
            "error" => false,
            "attempts" => 2
          );
          http_response_code(200);
          echo json_encode($response);
          // check for the owner ends
        } else {
          $is_available_query = "SELECT COUNT(*) AS is_attempts FROM video_attempts WHERE uid = '$uid' AND lesson_id = '$lesson_id' AND video_id = '$video_id'";
          $is_available_statement = $db->prepare($is_available_query);
          $is_available_statement->execute();
          $is_available_row = $is_available_statement->fetch();
          $is_attempts = 0;
          $attempts = 0;
          if (!empty($is_available_row["is_attempts"])) {
            $is_attempts = json_encode($is_available_row["is_attempts"]);
  
            $attempts_query = "SELECT attempts FROM video_attempts WHERE uid = '$uid' AND lesson_id = '$lesson_id' AND video_id = '$video_id'";
            $attempts_statement = $db->prepare($attempts_query);
            $attempts_statement->execute();
            $attempts_row = $attempts_statement->fetch();
            $attempts = json_encode($attempts_row["attempts"]);
            $response = array(
              "status" => "success",
              "error" => false,
              "attempts" => (int)$attempts
            );
          } else {
            $add_attempts_query = "INSERT INTO video_attempts (uid, lesson_id, video_id, attempts) VALUES('$uid', '$lesson_id', '$video_id', 2)";
            $add_attempts_statement = $db->prepare($add_attempts_query);
            $add_attempts_statement->execute();
            $response = array(
              "status" => "success",
              "error" => false,
              "attempts" => 2
            );
          }
          http_response_code(200);
          echo json_encode($response);
        }
      }
    }
  }
  $EnrollLessons = new EnrollLessons;
  echo $EnrollLessons->enrollToLesson();
?>