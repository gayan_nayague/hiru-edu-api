<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class Lessons {
    function getAllPagedLessons() {
        $data = json_decode(file_get_contents("php://input"));
        $batch_id = htmlspecialchars($data->batch_id, ENT_QUOTES, 'UTF-8');
        $per_page = htmlspecialchars($data->per_page, ENT_QUOTES, 'UTF-8');
        $offset = htmlspecialchars($data->offset, ENT_QUOTES, 'UTF-8');

        $db = new Connect;
        $query = "SELECT
        l.id AS l_id,
        l.created_at AS l_created,
        l.updated_at AS l_updated,
        l.user_uid AS l_uid,
        l.title AS l_title,
        l.short_description AS l_desc,
        l.thumbnail_id AS l_thumb_id,
        t.id AS t_id,
        t.file_name AS t_f_name
        FROM lessons AS l
        LEFT JOIN lesson_thumbnails as t
        ON l.thumbnail_id = t.id
        WHERE l.batch_id = '$batch_id'
        ORDER BY l.created_at DESC
        LIMIT $per_page
        OFFSET $offset";

        $statement = $db->prepare($query);
        $statement->execute();
        $lessonsData = array();
        while($OutputData=$statement->fetch(PDO::FETCH_ASSOC)){
          $lessonsData[$OutputData['l_id']]=array(
           'l_id'=> $OutputData['l_id'],
           'l_created' => $OutputData['l_created'],
           'l_updated' => $OutputData['l_updated'],
           'l_uid' => $OutputData['l_uid'],
           'l_title' => $OutputData['l_title'],
           'l_desc' => $OutputData['l_desc'],
           't_f_name' => $OutputData['t_f_name'],
          );
        }
        $lessonsData = array_values($lessonsData);

        $lesson_count_query = "SELECT COUNT(*) AS lesson_count FROM lessons WHERE batch_id = '$batch_id'";
        $lesson_count_statement = $db->prepare($lesson_count_query);
        $lesson_count_statement->execute();
        $lesson_count_row = $lesson_count_statement->fetch();

        $lessons_data = (object) [
          'lesson_count'=> $lesson_count_row['lesson_count'],
          'lessons' => $lessonsData
        ];

        return json_encode($lessons_data);
    }
  }
  $Lessons = new Lessons;
  echo $Lessons->getAllPagedLessons();
?>