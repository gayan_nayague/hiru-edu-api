<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class Lessons {
    function getLessonVideo() {
      $data = json_decode(file_get_contents("php://input"));
      $uid = htmlspecialchars($data->uid, ENT_QUOTES, 'UTF-8');
      $lesson_id = htmlspecialchars($data->lesson_id, ENT_QUOTES, 'UTF-8');
      $video_id = htmlspecialchars($data->video_id, ENT_QUOTES, 'UTF-8');

      if ( empty($uid) ) {
        http_response_code(400);
        echo json_encode(array("message" => "uid is undefined."));
      } else if ( empty($lesson_id) ) {
        http_response_code(400);
        echo json_encode(array("message" => "lesson is undefined."));
      } else if ( empty($video_id) ) {
        http_response_code(400);
        echo json_encode(array("message" => "video is undefined."));
      } else {
        $db = new Connect;
        
        // check for the owner begins
        $owner_query = "SELECT COUNT(*) AS is_owner FROM lessons
        WHERE user_uid = '$uid' AND id = '$lesson_id'";
        $owner_statement = $db->prepare($owner_query);
        $owner_statement->execute();
        $owner_row = $owner_statement->fetch();
        $is_owner = false;
        if (!empty($owner_row["is_owner"])) {
          $is_owner = true;
        }
        // check for the owner ends

        $query = "SELECT COUNT(*) AS is_enrolled FROM enrolled_lessons
        WHERE user_uid = '$uid' AND lesson_id = '$lesson_id'";

        $statement = $db->prepare($query);
        $statement->execute();
        $row = $statement->fetch();
        $is_enrolled = json_encode($row["is_enrolled"]);
        $is_enrolled = (int)$is_enrolled;

        if ($is_enrolled == 1 || $is_owner) {
          $lesson_query = "SELECT id, file_name, video_title FROM lesson_videos WHERE id = '$video_id'";
          $lesson_statement = $db->prepare($lesson_query);
          $lesson_statement->execute();
          $lesson_row = $lesson_statement->fetch();
          
          $video = (object) [
            'id'=> $lesson_row['id'],
            'file_name'=> $lesson_row['file_name'],
            'video_title'=> $lesson_row['video_title']
          ];
          http_response_code(200);
          return json_encode($video);
        }
      }
    }
  }
  $Lessons = new Lessons;
  echo $Lessons->getLessonVideo();
?>