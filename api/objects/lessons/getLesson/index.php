<?php
  require_once __DIR__ . '../../../../config/core.php';
  require_once __DIR__ . '../../../../config/database.php';
  class Lessons {
    function getAllPagedLessons() {
        $data = json_decode(file_get_contents("php://input"));
        $id = htmlspecialchars($data->id, ENT_QUOTES, 'UTF-8');

        $db = new Connect;
        $query = "SELECT
        l.id AS l_id,
        l.created_at AS l_created,
        l.updated_at AS l_updated,
        l.user_uid AS l_uid,
        l.title AS l_title,
        l.short_description AS l_desc,
        l.thumbnail_id AS l_thumb_id,
        t.id AS t_id,
        t.file_name AS t_f_name
        FROM lessons AS l
        LEFT JOIN lesson_thumbnails as t
        ON l.thumbnail_id = t.id
        WHERE l.id = '$id'";
        $videos_query = "SELECT * FROM lesson_videos WHERE lesson_id = '$id'";
        $tutes_query = "SELECT * FROM lesson_tute WHERE lesson_id = '$id'";

        $statement = $db->prepare($query);
        $videos_statement = $db->prepare($videos_query);
        $tutes_statement = $db->prepare($tutes_query);

        $statement->execute();
        $videos_statement->execute();
        $tutes_statement->execute();

        $videosData = array();
        while($OutputData=$videos_statement->fetch(PDO::FETCH_ASSOC)){
          $videosData[$OutputData['id']]=array(
           'id'=> $OutputData['id'],
           'created_at' => $OutputData['created_at'],
           'lesson_id' => $OutputData['lesson_id'],
           'video_title' => $OutputData['video_title'],
          //  'file_name' => $OutputData['file_name'],
           'display_order' => $OutputData['display_order']
          );
        };
        $videosData = array_values($videosData);

        $tutesData = array();
        while($OutputData=$tutes_statement->fetch(PDO::FETCH_ASSOC)){
          $tutesData[$OutputData['id']]=array(
           'id'=> $OutputData['id'],
           'created_at' => $OutputData['created_at'],
           'lesson_id' => $OutputData['lesson_id'],
           'tute_title' => $OutputData['tute_title'],
           'file_name' => $OutputData['file_name'],
           'display_order' => $OutputData['display_order']
          );
        };
        $tutesData = array_values($tutesData);

        $row = $statement->fetch();

        $author_uid = $row['l_uid'];
        $author_query = "SELECT u.firstname AS f_name, u.lastname AS l_name, u.email AS email, u.id AS user_id, u.photoURL AS photo FROM users AS u WHERE uid = '$author_uid'";
        $author_statement = $db->prepare($author_query);
        $author_statement->execute();
        $author_row = $author_statement->fetch();

        $lesson = (object) [
          'l_id'=> $row['l_id'],
          'l_created' => $row['l_created'],
          'l_updated' => $row['l_updated'],
          'l_uid' => $row['l_uid'],
          'l_title' => $row['l_title'],
          'l_desc' => $row['l_desc'],
          't_f_name' => $row['t_f_name'],
          'videos' => $videosData,
          'tutes' => $tutesData,
          'f_name' => $author_row['f_name'],
          'l_name' => $author_row['l_name'],
          'email' => $author_row['email'],
          'user_id' => $author_row['user_id'],
          'photo' => $author_row['photo']
        ];
        return json_encode($lesson);
    }
  }
  $Lessons = new Lessons;
  echo $Lessons->getAllPagedLessons();
?>